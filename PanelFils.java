import java.awt.*;
import javax.swing.*;

public class PanelFils extends JPanel {
	
	public PanelFils(Heros heros1, Heros heros2) {
		
		setLayout(new BorderLayout(20, 20));
		
		add(new PanelFilsAffichageHeros(new Heros[]{heros1, heros2}), BorderLayout.NORTH);
		add(new PanelFilsLogCombat(heros1, heros2), BorderLayout.SOUTH);
		
		setBackground(new Color(255,255,255));
		
	}
	
}
