import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class PanelFilsLogCombat extends JPanel implements ActionListener
{
	JButton boutonDemarrerCombat;
		
	public PanelFilsLogCombat(Heros heros1, Heros heros2)
	{
		setLayout(new BorderLayout(20, 20));
		
		JTextArea logCombat = new JTextArea();

		boutonDemarrerCombat = new JButton("Démarrer le combat");
		
		add(logCombat, BorderLayout.WEST);
		add(boutonDemarrerCombat, BorderLayout.EAST);
		
		boutonDemarrerCombat.addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent parEvt)
	{
		if (parEvt.getSource() == boutonDemarrerCombat)
		{
			System.out.println("FIGHTTTTTT !!!");
		}
	}
}
