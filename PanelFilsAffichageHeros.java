import java.awt.*;
import javax.swing.*;

public class PanelFilsAffichageHeros extends JPanel
{
	public PanelFilsAffichageHeros(Heros[] tabHeros)
	{
		int i;
		
		setLayout(new BorderLayout(20, 20));
		
		JTextArea logCombat = new JTextArea();
		
		for (i=0;i!=tabHeros.length;i++)
		{
			logCombat.append("=== HEROS " + (i + 1) + " ===\n");
			logCombat.append("Nom : " + tabHeros[i].getNom() + "\n");
			logCombat.append("Classe : " + tabHeros[i].getClasse() + "\n");
		}
		
		add(logCombat);
		
	}
}
