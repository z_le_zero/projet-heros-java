public class Heros
{
	//commentaire de Nicolas de test 
	private String chNom;
	private String chClasse;
	private String chGenre;
	private int chPointsDeVie = 50;
	private int chPointsDeVieInitial = chPointsDeVie;
	private int chAttaque = 10;
	private int chDefense = 10;
	private int chDommages = 10;
	
	public Heros(String parNom, String parClasse, String parGenre)
	{
		chNom = parNom;
		chClasse = parClasse;
		chGenre = parGenre;
	}
	
	public Heros(String parNom, String parClasse, String parGenre, int parPointsDeVie, int parAttaque, int parDefense, int parDommages)
	{
		chNom = parNom;
		chClasse = parClasse;
		chGenre = parGenre;
		chPointsDeVie = parPointsDeVie;
		chPointsDeVieInitial = parPointsDeVie;
		chAttaque = parAttaque;
		chDefense = parDefense;
		chDommages = parDommages;
	}
	
	public void Soigner()
	{
		chPointsDeVie = chPointsDeVieInitial;
	}
	
	public boolean estVivant()
	{
		return chPointsDeVie > 0;
	}
	
	//GETTERS
	
	public String getNom()
	{
		return chNom;
	}
	
	public String getClasse()
	{
		return chClasse;
	}
	
	public String getGenre()
	{
		return chGenre;
	}
	
	public int getPV()
	{
		return chPointsDeVie;
	}
	
	public int getDefense()
	{
		return chDefense;
	}
	
	public int getAttaque()
	{
		return chAttaque;
	}
	
	public int getDommage()
	{
		return chDommages;
	}
	
	// SETTERS
	
	public void setNom(String parNom)
	{
		chNom = parNom;
	}
	
	public void setClasse(String parClasse)
	{
		chClasse = parClasse;
	}
	
	public void setGenre(String parGenre)
	{
		chGenre = parGenre;
	}
	
	public void setPV(int parNouveauPv)
	{
		chPointsDeVie = parNouveauPv;
	}
	
	public void setAttaque(int parAttaque)
	{
		chAttaque = parAttaque;
	}
	
	public void setDefense(int parDefense)
	{
		chDefense = parDefense;
	}
	
	public void setDommage(int parDommage)
	{
		chDommages = parDommage;
	}
	
	public String toStringStat() {
		return "[" + chNom + "/" + chClasse + "/" + chGenre + "/ PV: " + chPointsDeVie + "/ATT: " + chAttaque + "/DEF: " + chDefense + "/DMG: " + chDommages + "]";
	}
	public void Attaquer(Heros parDefenseur)
	{
		int totaldmg = (int)(chDommages * Math.random());
		if (Math.random() <= 0.15) {
			System.out.println(this + " attaque " + parDefenseur.chNom + " mais " + parDefenseur.chNom + " a esquivé\n");
		}
		else if (Math.random() >= (float)this.chAttaque/(float) ( this.chAttaque + parDefenseur.getDefense() ) ) {
			parDefenseur.setPV(parDefenseur.getPV() - (totaldmg) );
			System.out.println(this + " attaque " + parDefenseur.chNom + " et inflige " + totaldmg + " points de dommage");
			System.out.println("\n	Nouvelles stats : \n");
			System.out.println("		Joueur 1 : " + this.toStringStat());
			System.out.println("		Joueur 2 : " + parDefenseur.toStringStat()+"\n");
		}
		else 
			System.out.println(this + " attaque " + parDefenseur.chNom + " mais ce n'est pas efficace\n");
	}
	
	public String toString()
	{
		if (chGenre == "f")
			return chNom + " la " + chClasse;
		else
			return chNom + " le " + chClasse;
	}
	
	public void deadlyFight(Heros Defheros){
		int tour = 1;
		while (this.chPointsDeVie > 0 && Defheros.chPointsDeVie > 0) {
			System.out.println("\n[TOURS: " + tour + "]\n ");
			this.Attaquer(Defheros);
			Defheros.Attaquer(this);
			tour++;
		}
		if (this.chPointsDeVie > 0)
			System.out.println(this.chNom + " a gagné");
		else if (Defheros.chPointsDeVie > 0)
			System.out.println(Defheros.chNom + " a gagné");
		else
			System.out.println(Defheros.chNom + " et " + this.chNom + " ont tous les deux perdus");
	}
	
	public static void main(String[] args)
	{
		Heros monHeros = new Heros("Bob", "Magicien", "h",350,15,54,83);
		System.out.println(monHeros);
		Heros maHeroine = new Heros("Zoé", "Barbare", "f",500,23,45,65);
		System.out.println(maHeroine);
		monHeros.deadlyFight(maHeroine);
	}
	
}
