import java.awt.*;
import javax.swing.*;

public class FenetreMere extends JFrame
{
	public FenetreMere(String parTitre, Heros heros1, Heros heros2)
	{
		super(parTitre); //On met le titre de la fenêtre
		
		PanelFils contentPane = new PanelFils(heros1, heros2);
		
		setContentPane(contentPane);
		
		contentPane.setBackground(new Color(100, 60, 120));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(400, 600);
		setVisible(true);
		setLocation(200, 300);
	}
	
	public Insets getInsets() //Défini les marges
	{
		return new Insets(50, 15, 15, 15);
	}
	
	public static void main (String[] args)
	{
		Heros monHeros = new Heros("Bob", "Magicien", "h");
		Heros maHeroine = new Heros("Zoé", "Barbare", "f");
		new FenetreMere("Ultimate Heros Fighter Z", monHeros, maHeroine);
	}
}
